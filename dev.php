<?php

// 加载自动载入
require_once __DIR__ . '/vendor/autoload.php';

use TekinHttp\HttpClient;
use TekinHttp\HttpRequest;
use TekinHttp\Injector;
use TekinHttp\HttpException;
use TekinHttp\MyEnvironment;


if (!function_exists('dd')) {
    function dd($data)
    {
        echo "<pre>" . print_r($data, true) . "</pre>";
        exit;
    }
}
if (!function_exists('pp')) {
    function pp($data)
    {
        echo "<pre>" . print_r($data, true) . "</pre>";
    }
}
if (!function_exists('vd')) {
    function vd($arr)
    {
        echo "<pre>";
        var_dump($arr);
        echo "</pre>";
        die;
    }
}


class LogInjector implements Injector
{
    public function inject($httpRequest)
    {
        // do something with
        pp($httpRequest);
    }
}
class BasicInjector implements Injector
{
    public function inject($httpRequest)
    {
        // 注意这里的path会修改掉HttpRequest请求对象初始化是的path,即 /auth
        $httpRequest->path = "/example/1690812897291";

    }
}

$env = new MyEnvironment('http://rap2api.taobao.org/app/mock/300755');

$client = new HttpClient($env);

// 自定义请求注入1
$logInjector = new LogInjector();
$client->addInjector($logInjector);

// 自定义请求注入2
$basicInj = new BasicInjector();
$client->addInjector($basicInj);


$req = new HttpRequest("/auth", "POST");

// The "; boundary=--..." will be added by the Multipart serializer when serializing
// the body
$req->headers["Content-Type"] = "multipart/form-data";
$file = fopen(__DIR__ . '/tests/unit/Serializer/sample.txt', 'rb');
$body = [];
$body["file1"] = $file;
$body["key"] = "value";
$req->body = $body;

// 发送普通文本请求
// $req->headers["Content-Type"] = "text/plain";
// $req->body = "some data";

// 不发送任何请求体
// $req = new HttpRequest("/path", "POST");
// $client->execute($req);

// 自定义请求UA
$req->headers["User-Agent"] = "Example user-agent";
// 自定义请求头信息
$req->headers["Custom-Header"] = "Custom value";

try {
    $response = $client->execute($req);

    dd($response->result);

} catch (HttpException $e) {
    $statusCode = $e->response->statusCode;
    $headers = $e->response->headers;
    $body = $e->response->result;
}