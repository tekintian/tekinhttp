<?php

namespace TekinHttp;

class MyEnvironment implements Environment {
	/**
	 * @var string
	 */
	private $baseUrl;

	public function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	public function baseUrl() {
		return $this->baseUrl;
	}
}