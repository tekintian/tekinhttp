## Tekin HttpClient

TekinHttp is a generic HTTP Client.

In it's simplest form, an [`HttpClient`](lib/TekinHttp/HttpClient.php) exposes an `execute` method which takes an [HTTP request](lib/TekinHttp/HttpRequest.php), executes it against the domain described in an [Environment](lib/TekinHttp/Environment.php), and returns an [HTTP response](lib/TekinHttp/HttpResponse.php).

## how to use

install lib
~~~sh
composer require tekintian/tekinhttp
~~~

~~~php
// 加载自动载入
require_once __DIR__ . '/vendor/autoload.php';

$env = new TekinHttp\MyEnvironment('http://rap2api.taobao.org/app/mock/300755');
$client = new TekinHttp\HttpClient($env);
$req = new TekinHttp\HttpRequest("/auth", "POST");

try {
    $response = $client->execute($req);

    dd($response->result);

} catch (TekinHttp\HttpException $e) {
    $statusCode = $e->response->statusCode;
    $headers = $e->response->headers;
    $body = $e->response->result;
}
~~~


sample code dev.php

使用vscode打开，调试里面点击 Launch built-in server and debug

打开下面网址即可查看效果
http://localhost:8080/dev.php




### Environment

An [`Environment`](./lib/TekinHttp/environment.php) describes a domain that hosts a REST API, against which an `HttpClient` will make requests. `Environment` is a simple interface that wraps one method, `baseUrl`.

```php
$myenv = new MyEnvironment('https://example.com');
```

### Requests

HTTP requests contain all the information needed to make an HTTP request against the REST API. Specifically, one request describes a path, a verb, any path/query/form parameters, headers, attached files for upload, and body data.

### Responses

HTTP responses contain information returned by a server in response to a request as described above. They are simple objects which contain a status code, headers, and any data returned by the server.

```php
$request = new HttpRequest("/path", "GET");
$request->body[] = "some data";

$response = $client->execute($req);

$statusCode = $response->statusCode;
$headers = $response->headers;
$data = $response->result;
```

### Injectors

Injectors are blocks that can be used for executing arbitrary pre-flight logic, such as modifying a request or logging data. Injectors are attached to an `HttpClient` using the `addInjector` method.

The `HttpClient` executes its injectors in a first-in, first-out order, before each request.

```php
class LogInjector implements Injector
{
    public function inject($httpRequest)
    {
        // Do some logging here
    }
}

$logInjector = new LogInjector();
$client = new HttpClient($myenv);
$client->addInjector($logInjector);
...
```

### Error Handling

`HttpClient#execute` may throw an `Exception` if something went wrong during the course of execution. If the server returned a non-200 response, [IOException](lib/TekinHttp/IOException.php) will be thrown, that will contain a status code and headers you can use for debugging.

```php
try
{
    $client->execute($req);
}
catch (HttpException $e)
{
    $statusCode = $e->response->statusCode;
    $headers = $e->response->headers;
    $body = $e->response->result;
}
```


This base code from PayPal PHP HttpClient

